﻿using UnityEngine;
using System.Collections;

public class PressAny : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.anyKeyDown) {
            Application.LoadLevel(0);
            Choose.mouseUse = false;
            Choose.keyboardUse = false;
            Choose.controllerUse = false;
        }
	}
}

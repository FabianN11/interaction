﻿using UnityEngine;
using System.Collections;

public class Choose : MonoBehaviour {
    static public bool mouseUse = false;
    static public bool keyboardUse = false;
    static public bool controllerUse = false;
	public bool generateLog = false;
	static public bool SgenerateLog;

    // Use this for initialization
    void Start () {
		SgenerateLog = generateLog;
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetMouseButtonDown(0))
        {
            mouseUse = true;
            Application.LoadLevel(3);
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            keyboardUse = true;
            Application.LoadLevel(3);
        }

        if(Input.GetKeyDown(KeyCode.Joystick1Button1)) 
        {
            controllerUse = true;
            Application.LoadLevel(3);
        }

        if((Input.GetKeyDown(KeyCode.Escape)) || (Input.GetMouseButtonDown(1)) || (Input.GetKeyDown(KeyCode.Joystick1Button0)))
        {
            Application.Quit();
        }
	}
}

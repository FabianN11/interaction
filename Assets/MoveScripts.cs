﻿using UnityEngine;
using System.Collections;

public class MoveScripts : MonoBehaviour {
	public float moveSpeed = 2.0f;
	public float wallLeft = -1.0f;
	public float wallRight = 1.0f;
	public float wallUp = 1.0f;
	public float wallDown = -1.0f;
	public bool moveHorizontal = false;
	public bool moveVertical = false;

	float moveDirectionX = 0.0f;
	float moveDirectionY = 0.0f;
	
	// Use this for initialization
	void Start () {
		if (moveHorizontal) {
			moveDirectionX = 1.0f;
		}

		if (moveVertical) {
			moveDirectionY = 1.0f;
		}
		
	}
	
	// Update is called once per frame
	void Update () {
		
		GetComponent<Rigidbody2D>().velocity = new Vector2 (moveDirectionX * moveSpeed, moveDirectionY * moveSpeed);

		if (moveHorizontal) {
			if ((moveDirectionX > 0.0f) && (transform.position.x > wallRight)) {
				moveDirectionX *= -1;
			} else if ((moveDirectionX < 0.0f) && (transform.position.x < wallLeft)) {
				moveDirectionX *= -1;
			}
		}

		if (moveVertical) {
			if ((moveDirectionY > 0.0f) && (transform.position.y > wallUp)) {
				moveDirectionY *= -1;
			} else if ((moveDirectionY < 0.0f) && (transform.position.y < wallDown)) {
				moveDirectionY *= -1;
			}
		}

	}

}

﻿using UnityEngine;
using System.Collections;

public class Control : MonoBehaviour {
    public float maxSpeed;
	private float playedTime;
	private string playUtil;

	// Use this for initialization
	void Start () {
		playedTime = 0.0f;

		if (Choose.keyboardUse == true) {
			playUtil = "Keyboard";
		}

		if (Choose.controllerUse == true) {
			playUtil = "Controller";
		}

		if (Choose.mouseUse == true) {
			playUtil = "Mouse";
		}
	
	}
	
	// Update is called once per frame
    void Update()
    {
		playedTime += Time.deltaTime;

        //keyboard
        if (Choose.keyboardUse == true)
        {
            float moveX = Input.GetAxis("HorizontalK");
            float moveY = Input.GetAxis("VerticalK");

            GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * maxSpeed, moveY * maxSpeed);

            /*if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.LoadLevel(0);
                Choose.keyboardUse = false;
            }*/
        }
        //controller
        if (Choose.controllerUse == true)
        {
            float moveX = Input.GetAxis("HorizontalC");
            float moveY = Input.GetAxis("VerticalC");

            GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * maxSpeed, moveY * maxSpeed);

            /*if (Input.GetKeyDown(KeyCode.Joystick1Button0))
            {
                Application.LoadLevel(0);
                Choose.controllerUse = false;
            }*/
        }
        //mouse
        if (Choose.mouseUse == true)
        {
            Vector3 moveM = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            moveM.z = transform.position.z;
            transform.position = Vector3.MoveTowards(transform.position, moveM, maxSpeed * Time.deltaTime);

            /*if (Input.GetMouseButtonDown(1))
            {
                Application.LoadLevel(0);
                Choose.mouseUse = false;
            }*/
        }

        
    }

    void OnTriggerEnter2D (Collider2D coll)
    {
        if(coll.tag == "Bad") {
			if((Application.isEditor)&& (Choose.SgenerateLog)) {
			System.IO.File.AppendAllText(Application.dataPath + "/Logs/" + System.DateTime.Now.ToString("yyyy-MM-dd") + ".log",
			                             "\n" + "Time of Failure: " + System.DateTime.Now.ToString("HH:mm:ss") + "\n" +
			                             "Playtime: " + playedTime + " sec." + "\n" + 
			                             "Used: " + playUtil + "\n" +
			                             "Failed at: " + "\n" + "X: " + transform.position.x + "\n" + "Y: " + transform.position.y + "\n"
				                             );}

            Application.LoadLevel(2);
        }

        if (coll.tag == "Good")
        {
			if((Application.isEditor)&& (Choose.SgenerateLog)) {
				System.IO.File.AppendAllText(Application.dataPath + "/Logs/" + System.DateTime.Now.ToString("yyyy-MM-dd") + ".log",
				                             "\n" + "Time of Winning: " + System.DateTime.Now.ToString("HH:mm:ss") + "\n" +
				                             "Playtime: " + playedTime + " sec." + "\n" + 
				                             "Used: " + playUtil + "\n"
				                             );}
            Application.LoadLevel(1);
        }
    }
}
